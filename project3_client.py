# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!
#
# Author: Kiran Hegde and Gautham KA
# Date: 11/11/2018
# EID Project3 Submission

from PyQt5 import QtCore, QtGui, QtWidgets
import Adafruit_DHT as sens
import sys
import time
import matplotlib.pyplot as plt
import sqlite3 as sq
import datetime
import json
import boto3
import matplotlib.dates as md

array_curT = []
array_minT = []
array_maxT = []
array_avgT = []
array_curH = []
array_minH = []
array_maxH = []
array_avgH = []
array_t = []

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(649, 496)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.textBrowser = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser.setGeometry(QtCore.QRect(140, 90, 111, 31))
        self.textBrowser.setObjectName("textBrowser")
        self.textBrowser_2 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_2.setGeometry(QtCore.QRect(270, 90, 111, 31))
        self.textBrowser_2.setObjectName("textBrowser_2")
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(140, 60, 67, 21))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralWidget)
        self.label_2.setGeometry(QtCore.QRect(290, 60, 67, 21))
        self.label_2.setObjectName("label_2")
        self.textBrowser_5 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_5.setGeometry(QtCore.QRect(270, 130, 111, 31))
        self.textBrowser_5.setObjectName("textBrowser_5")
        self.textBrowser_6 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_6.setGeometry(QtCore.QRect(140, 130, 111, 31))
        self.textBrowser_6.setObjectName("textBrowser_6")
        self.textBrowser_8 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_8.setGeometry(QtCore.QRect(270, 170, 111, 31))
        self.textBrowser_8.setObjectName("textBrowser_8")
        self.textBrowser_9 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_9.setGeometry(QtCore.QRect(140, 170, 111, 31))
        self.textBrowser_9.setObjectName("textBrowser_9")
        self.textBrowser_11 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_11.setGeometry(QtCore.QRect(270, 210, 111, 31))
        self.textBrowser_11.setObjectName("textBrowser_11")
        self.textBrowser_12 = QtWidgets.QTextBrowser(self.centralWidget)
        self.textBrowser_12.setGeometry(QtCore.QRect(140, 210, 111, 31))
        self.textBrowser_12.setObjectName("textBrowser_12")
        self.label_4 = QtWidgets.QLabel(self.centralWidget)
        self.label_4.setGeometry(QtCore.QRect(50, 100, 41, 21))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralWidget)
        self.label_5.setGeometry(QtCore.QRect(50, 140, 71, 21))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralWidget)
        self.label_6.setGeometry(QtCore.QRect(50, 210, 71, 21))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralWidget)
        self.label_7.setGeometry(QtCore.QRect(50, 180, 71, 20))
        self.label_7.setObjectName("label_7")
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 649, 27))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.Plot = QtWidgets.QPushButton(MainWindow)
        self.Plot.setEnabled(True)
        self.Plot.setGeometry(QtCore.QRect(150,300,61,31))
        self.Plot.setObjectName("Plot")
        self.Plot.clicked.connect(self.button_clicked)

        self.Plot1 = QtWidgets.QPushButton(MainWindow)
        self.Plot1.setEnabled(True)
        self.Plot1.setGeometry(QtCore.QRect(300,300,61,31))
        self.Plot1.setObjectName("Plot")
        self.Plot1.clicked.connect(self.button_clicked1)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Temp"))
        self.label_2.setText(_translate("MainWindow", "Humidity"))
        self.label_4.setText(_translate("MainWindow", "Last"))
        self.label_5.setText(_translate("MainWindow", "Maximum"))
        self.label_6.setText(_translate("MainWindow", "Average"))
        self.label_7.setText(_translate("MainWindow", "Minimum"))
        self.Plot.setText(_translate("Dialog", "Plot_F"))
        self.Plot1.setText(_translate("Dialog", "Plot_C"))

    def myFunction(variable):
        sqs = boto3.client('sqs', region_name = 'us-west-2',
                   aws_access_key_id='',
                   aws_secret_access_key='',
                   aws_session_token='')

        url = sqs.get_queue_url(QueueName='project3_q.fifo')
        print(url['QueueUrl'])
        response = sqs.receive_message(
               QueueUrl =  url['QueueUrl'],
               AttributeNames= [
                    'SentTimestamp'
               ],
               MaxNumberOfMessages = 10,
               MessageAttributeNames=[
                    'All'
               ],
               VisibilityTimeout = 0,
               WaitTimeSeconds = 0
               )

        message = response['Messages'][0]
        receipt_handle = message['ReceiptHandle']




        response1 = sqs.receive_message(
               QueueUrl =  url['QueueUrl'],
               AttributeNames= [
                    'SentTimestamp'
               ],
               MaxNumberOfMessages = 10,
               MessageAttributeNames=[
                    'All'
               ],
               VisibilityTimeout = 0,
               WaitTimeSeconds = 0
               )

        message = response1['Messages'][0]
        receipt_handle = message['ReceiptHandle']

        response2 = sqs.receive_message(
               QueueUrl =  url['QueueUrl'],
               AttributeNames= [
                    'SentTimestamp'
               ],
               MaxNumberOfMessages = 10,
               MessageAttributeNames=[
                    'All'
               ],
               VisibilityTimeout = 0,
               WaitTimeSeconds = 0
               )


        response['Messages'] = (response['Messages'] + response1['Messages'] + response2['Messages'])

        for i in range (0,30):
            message = response['Messages'][i]
            #Spliting string and taking timestamp value
            body = response['Messages'][i]['Body']
            #print('\n The body content is %s' %body)
            x = body.split(",")
            print('\n The sliced string is %s' %x[0])
            array_t.append(x[0])
            if(variable == 1):
                a = ((9.0/5.0) * (float(x[1]))) + 32.0
                array_curT.append(a)
                b = ((9.0/5.0) * (float(x[2]))) + 32.0
                array_minT.append(b)
                c = ((9.0/5.0) * (float(x[3]))) + 32.0
                array_avgT.append(c)
                d = ((9.0/5.0) * (float(x[4]))) + 32.0
                array_maxT.append(d)
            elif (variable == 2):
                a = float(x[1])
                array_curT.append(a)
                b = float(x[2])
                array_minT.append(b)
                c = float(x[3])
                array_avgT.append(c)
                d = float(x[4])
                array_maxT.append(d)

            array_curH.append(float(x[5]))
            array_minH.append(float(x[6]))
            array_avgH.append(float(x[7]))
            array_maxH.append(float(x[8]))


    def button_clicked(self):
        array_curT.clear()
        array_minT.clear()
        array_maxT.clear()
        array_avgT.clear()
        array_curH.clear()
        array_minH.clear()
        array_maxH.clear()
        array_avgH.clear()
        array_t.clear()
        _trans = QtCore.QCoreApplication.translate
        Ui_MainWindow.myFunction(1)
        self.textBrowser.setText(_trans("Dialog",str(array_curT[-1])))
        self.textBrowser_6.setText(_trans("Dialog",str(array_maxT[-1])))
        self.textBrowser_9.setText(_trans("Dialog",str(array_minT[-1])))
        self.textBrowser_12.setText(_trans("Dialog",str(array_avgT[-1])))
        self.textBrowser_8.setText(_trans("Dialog",str(array_minH[-1])))
        self.textBrowser_5.setText(_trans("Dialog",str(array_maxH[-1])))
        self.textBrowser_2.setText(_trans("Dialog",str(array_curH[-1])))
        self.textBrowser_11.setText(_trans("Dialog",str(array_avgH[-1])))


        fig = plt.figure()
        ax = fig.add_subplot(311)
        p1 = ax.plot(array_t,array_curT,'r-')
        p2 = ax.plot(array_t,array_minT,'m-')
        p3 = ax.plot(array_t,array_maxT,'k-')
        p4 = ax.plot(array_t,array_avgT,'y-')
        #ax.xlabel('Time')
        #ax.ylabel('Temp(F)')

        # adding number of Messages received and start and end of messages in plots
        fig.suptitle("Messages Received: 10 ( Start Time:" +str(array_t[0]) +"END Time:"+ str(array_t[9])+ " )")
        ax.set_title('Temperature Variance')
        ax.grid()
        #ax.legend(loc = 'best')
        ax.legend((p1[0],p2[0],p3[0],p4[0]), ('Cur Temp','Min Temp','Avg Temp','Max Temp'))

        ax1 = fig.add_subplot(313)
        p1 = ax1.plot(array_t,array_curH,'r-')
        p2 = ax1.plot(array_t,array_minH,'m-')
        p3 = ax1.plot(array_t,array_maxH,'k-')
        p4 = ax1.plot(array_t,array_avgH,'y-')
        #ax1.xlabel('Time')
        #ax1.ylabel('Humidity(%)')
        ax1.set_title('Humidity Variance')
        ax1.grid()
        #ax1.legend(loc = 'best')
        ax1.legend((p1[0],p2[0],p3[0],p4[0]), ('Cur Hum','Min Hum','Avg Hum','Max Hum'))

        plt.show()

    def button_clicked1(self):
        array_curT.clear()
        array_minT.clear()
        array_maxT.clear()
        array_avgT.clear()
        array_curH.clear()
        array_minH.clear()
        array_maxH.clear()
        array_avgH.clear()
        array_t.clear()
        _trans = QtCore.QCoreApplication.translate
        Ui_MainWindow.myFunction(2)
        self.textBrowser.setText(_trans("Dialog",str(array_curT[-1])))
        self.textBrowser_6.setText(_trans("Dialog",str(array_maxT[-1])))
        self.textBrowser_9.setText(_trans("Dialog",str(array_minT[-1])))
        self.textBrowser_12.setText(_trans("Dialog",str(array_avgT[-1])))
        self.textBrowser_8.setText(_trans("Dialog",str(array_minH[-1])))
        self.textBrowser_5.setText(_trans("Dialog",str(array_maxH[-1])))
        self.textBrowser_2.setText(_trans("Dialog",str(array_curH[-1])))
        self.textBrowser_11.setText(_trans("Dialog",str(array_avgH[-1])))


        fig = plt.figure()
        fig.suptitle("Messages Received: 10 ( Start Time:" +str(array_t[0]) +"END Time:"+ str(array_t[9])+ " )")
        ax = fig.add_subplot(311)
        p1 = ax.plot(array_t,array_curT,'r-')
        p2 = ax.plot(array_t,array_minT,'m-')
        p3 = ax.plot(array_t,array_maxT,'k-')
        p4 = ax.plot(array_t,array_avgT,'y-')
        #ax.xlabel('Time')
        #ax.ylabel('Temp(F)')
        ax.set_title('Temperature Variance')
        ax.grid()
        #ax.legend(loc = 'best')
        ax.legend((p1[0],p2[0],p3[0],p4[0]), ('Cur Temp','Min Temp','Avg Temp','Max Temp'))

        ax1 = fig.add_subplot(313)
        p1 = ax1.plot(array_t,array_curH,'r-')
        p2 = ax1.plot(array_t,array_minH,'m-')
        p3 = ax1.plot(array_t,array_maxH,'k-')
        p4 = ax1.plot(array_t,array_avgH,'y-')
        #ax1.xlabel('Time')
        #ax1.ylabel('Humidity(%)')
        ax1.set_title('Humidity Variance')
        ax1.grid()
        #ax1.legend(loc = 'best')
        ax1.legend((p1[0],p2[0],p3[0],p4[0]), ('Cur Hum','Min Hum','Avg Hum','Max Hum'))

        plt.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
