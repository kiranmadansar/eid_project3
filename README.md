# ECEN5053-002 Embedded Interface Design
# Project 3
* Amazon Web Server with temperature and Humidity sensor

### Kiran Hegde and Gautham KA

## Installation Instructions 
* Clone github repository from https://kiranmadansar@bitbucket.org/kiranmadansar/eid_project3.git
* Uses sqlite3 as the database
* Uses boto3 AWS Python SDK , can be installed by running `pip3 install boto3`
* Uses matplotlib for plotting graphs , can be installed by running `pip3 install matplotlib`

### Run Instructions
* run `python3 basicPubSub.py` to start the data publish to AWS
* run `python3 project3_client.py` to start the remote pi QT interface showing the graph of data retrived from SQS queue 


### Files in the directory
* basicPubSub.py reads data from the temperature sensor and publishes to AWS 
* database.py - database operations
* lambda_function.js - AWS lambda code for getting data from 
* project3_client.py - remote pi QT UI that gets data from SQS queue and plots graph 
* proj2_ui.py - Provides server side QT display and sensor data sending to AWS 


## References
* http://docs.aws.amazon.com/iot/latest/developerguide/iot-lambda-rule.html AWS help to setup IOT rule, and other AWS documentation pages
* http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/sqs-examples-send-receive-messages.html
* https://blog.runscope.com/posts/how-to-write-your-first-aws-lambda-function
* https://www.youtube.com/watch?v=y6W9QfiEY2E


